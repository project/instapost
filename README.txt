CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

Module InstaPost allows users to post nodes automatically to the online
mobile photo-sharing, video-sharing, and social networking service Instagram.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/kuzmenko/2744339

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2744339

REQUIREMENTS
------------

This module requires cURL library installed on the server.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Go to module settings page /admin/modules and enable Instapost module.

CONFIGURATION
-------------

 * Go to InstaPost settings page /admin/config/services/instapost and fill
   there user login and password from Instagram.

 * Save configuration.

 * Configure node type that should be posted to Instagram on
   /admin/structure/types/manage/[NODE_TYPE] page.

 * When creating node, just check "Post this node to Instagram" and data will be
   automatically sent to Instagram.
