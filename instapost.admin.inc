<?php

/**
 * @file
 * Contains instapost settings form.
 */

/**
 * Page callback. Return form with instapost main settings.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 *
 * @return mixed
 *   Result form.
 */
function instapost_admin_main_settings(array $form, array &$form_state) {
  // Application settings.
  $form['instapost_authorization'] = array(
    '#type' => 'fieldset',
    '#title' => t('Instagram Authorization settings'),
    '#collapsible' => TRUE,
    '#description' => t('You need to enter the data from the Instagram account through which you want to publish a node, to ensure that the module is working.'),
  );

  $form['instapost_authorization']['instapost_login'] = array(
    '#type' => 'textfield',
    '#title' => t('Login'),
    '#description' => t('Login of Instragram account.'),
    '#default_value' => variable_get('instapost_login', ''),
    '#required' => TRUE,
  );

  $form['instapost_authorization']['instapost_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#description' => t('Password of Instragram account.'),
    '#default_value' => variable_get('instapost_password', ''),
    '#required' => TRUE,
  );

  $form['#submit'][] = 'instapost_admin_main_settings_submit';

  return system_settings_form($form);
}

/**
 * Second step of getting access token.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 */
function instapost_admin_main_settings_submit(array &$form, array &$form_state) {
  // Define the user agent.
  $agent = instapost_generate_user_agent();

  // Define the GuID.
  $guid = instapost_generate_guid();

  // Set the device ID.
  $device_id = 'android-' . $guid;

  variable_set('instapost_agent', $agent);
  variable_set('instapost_guid', $guid);
  variable_set('instapost_device_id', $device_id);
}
