<?php

/**
 * @file
 * Contains node form alters.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function instapost_form_node_type_form_alter(&$form, &$form_state) {
  // Add instapost settings to node settings form.
  if (empty($form['#node_type']->type)) {
    return;
  }

  $node_type = $form['#node_type']->type;

  $form['instapost'] = array(
    '#type' => 'fieldset',
    '#title' => t('InstaPost settings'),
    '#group' => 'additional_settings',
  );

  // Enable setting.
  $form['instapost']['instapost_node_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable InstaPost'),
    '#default_value' => variable_get('instapost_node_enabled_' . $node_type, 0),
    '#description' => t('Enable or disable instagram posting for this node type.'),
  );

  // Enable default setting.
  $form['instapost']['instapost_node_enabled_default'] = array(
    '#type' => 'checkbox',
    '#title' => t('Checkbox "Post this node to Instagram" are checked by default'),
    '#default_value' => variable_get('instapost_node_enabled_default_' . $node_type, 0),
    '#description' => t('Check this if you want checkbox "Post this node to Instagram" in node forms was checked by default.'),
  );

  // Post if node updated.
  $form['instapost']['instapost_node_update'] = array(
    '#type' => 'checkbox',
    '#title' => t('Post image to Instagram when node was updated'),
    '#default_value' => variable_get('instapost_node_update_' . $node_type, 0),
    '#description' => t('Check this if you want post images to Instagram when node was updated.'),
  );

  // Get information about node fields.
  $fields_info = field_info_instances('node', $node_type);

  // Collect all image fields.
  $image_fields = array();
  foreach ($fields_info as $field_name => $field_instance) {
    if ($field_instance['widget']['module'] == 'image') {
      $image_fields[$field_name] = $field_instance['label'] . ' (' . $field_name . ')';
    }
  }

  // Message.
  $form['instapost']['instapost_body'] = array(
    '#type' => 'fieldset',
    '#title' => t('Message'),
    '#description' => t('Put here message to send with the image to Instagram.'),
  );

  $form['instapost']['instapost_body']['instapost_node_message_field'] = array(
    '#type' => 'textarea',
    '#title' => t('Message template'),
    '#default_value' => variable_get('instapost_node_message_field_' . $node_type, ''),
  );

  if (module_exists('token')) {
    // Message tokens.
    $form['instapost']['instapost_body']['instapost_node_token'] = array(
      '#theme' => 'token_tree_link',
      '#token_types' => array('node'),
      '#global_types' => FALSE,
      '#click_insert' => TRUE,
    );
  }

  // Message length.
  $form['instapost']['instapost_body']['instapost_node_message_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Cut message'),
    '#field_prefix' => t('Cut message if it has more than'),
    '#field_suffix' => t('symbols'),
    '#size' => 4,
    '#default_value' => variable_get('instapost_node_message_length_' . $node_type, INSTAPOST_MESSAGE_LENGTH),
    '#description' => t('Leave this field empty to not cut a message.'),
  );

  // Image source.
  $form['instapost']['instapost_node_image_field'] = array(
    '#type' => 'select',
    '#title' => t('Image source'),
    '#description' => t('Choose field from which to take the image to crosspost.'),
    '#options' => $image_fields,
    '#default_value' => variable_get('instapost_node_image_field_' . $node_type, 0),
  );
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function instapost_form_node_form_alter(&$form, &$form_state) {
  // Do not process node form if it is not contains node type.
  if (empty($form['#node']->type)) {
    return;
  }

  // Check node type and user access
  // to see whether we should process a node form.
  $type_enabled = variable_get('instapost_node_enabled_' . $form['#node']->type, 0);
  $user_access = user_access('post to instagram');

  // If node type matches all requirements we have to add new fields to form.
  if ($type_enabled && $user_access) {
    // Add Instapost fieldset to node add/edit form.
    $form['instapost'] = array(
      '#type' => 'fieldset',
      '#title' => t('InstaPost'),
      '#group' => 'additional_settings',
      '#weight' => -10,
    );

    $form['instapost']['instapost_post_this_node'] = array(
      '#type' => 'checkbox',
      '#title' => t('Post this node to Instagram'),
      '#default_value' => variable_get('instapost_node_enabled_default_' . $form['#node']->type, 0),
      '#description' => t('After you submit this node it will be posted to Instagram.'),
    );

    // Disable crosspost if cURL library is not installed on a server.
    if (!function_exists('curl_init')) {
      $form['instapost']['instapost_post_this_node']['#disabled'] = TRUE;
      $form['instapost']['instapost_post_this_node']['#value'] = 0;
      $form['instapost']['instapost_post_this_node']['#description'] = t("You can't crosspost nodes until cURL library is not installed on your server.");
    }
  }
}

/**
 * Form for sending single custom post to Instagram.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 *
 * @return array
 *   Built form.
 */
function instapost_send_single_post_form(array $form, array $form_state) {
  $form['message_image'] = array(
    '#type' => 'managed_file',
    '#title' => t('Image'),
    '#required' => TRUE,
    '#upload_location' => 'public://instapost/',
    '#upload_validators' => array(
      'file_validate_extensions' => array('jpg png jpeg'),
    ),
    '#theme' => 'image_widget',
  );

  $form['message_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
  );

  if (module_exists('token')) {
    // Message tokens.
    $form['message_body_token'] = array(
      '#theme' => 'token_tree',
      '#global_types' => TRUE,
      '#click_insert' => TRUE,
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );

  return $form;
}

/**
 * Submit for 'instapost_send_single_post_form' form.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 */
function instapost_send_single_post_form_submit(array $form, array &$form_state) {
  $image_url = '';
  $message = !empty($form_state['values']['message_body'])
    ? $form_state['values']['message_body']
    : '';

  if (!empty($form_state['values']['message_image'])) {
    $image_fid = $form_state['values']['message_image'];
    $image_file = file_load($image_fid);

    // Get real image path.
    if (class_exists('CurlFile')) {
      $image_url = new CurlFile(drupal_realpath($image_file->uri));
    }
    else {
      $image_url = '@' . drupal_realpath($image_file->uri);
    }
  }


  if (!empty($image_url)) {
    // Post data to Instagram.
    instapost_post_to_instagram($message, $image_url);
  }
}
